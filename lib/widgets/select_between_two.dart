import 'package:flutter/material.dart';

Color secondaryColor = Colors.teal[900];
Color primaryColor = Colors.red[400];
Color terciaryColor = Colors.black26;

class SelectBetweenTwo extends StatefulWidget {

  final String opcaoOne;
  final String opcaoTwo;
  final Function(bool value) selectedCallBack;

  SelectBetweenTwo({
    this.opcaoOne,
    this.opcaoTwo,
    this.selectedCallBack,
  });

  @override
  _SelectBetweenTwoState createState() => _SelectBetweenTwoState();
}

class _SelectBetweenTwoState extends State< SelectBetweenTwo> {

  ///SELECTEDS and NORMALS
  double _heigthWidgetNormal = 60;
  double _heigthWidgetSelected = 90;
  int _flexNormal = 1;
  int _flexSelected = 2;
  Color _colorWidgetSelected = primaryColor;
  Color _colorWidgetNormal = terciaryColor;

  ///VALUES PARA MODIFICAR
  double _heigthWidgetOne = 60;
  double _heigthWidgetTwo = 60;
  int _flexOne = 1;
  int _flexTwo = 1;
  Color _colorWidgetOne = terciaryColor;
  Color _colorWidgetTwo = terciaryColor;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        SizedBox(height: 10, width: 3,),
        Expanded(flex: _flexOne, child: GestureDetector(
          onTap: (){
            selectedWidget(1);
          },
          child: AnimatedContainer(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5.0),
              border: Border.all(color: terciaryColor, width: 1.0),
              color: _colorWidgetOne,
            ),
            height: _heigthWidgetOne,
            duration: Duration(milliseconds: 500),
            child: Center(child: Text(
              widget.opcaoOne,
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: secondaryColor
              ),
            )),
          ),
        )),
        SizedBox(height: 10, width: 3,),
        Expanded(flex: _flexTwo, child: GestureDetector(
          onTap: (){
            selectedWidget(2);
          },
          child: AnimatedContainer(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5.0),
              border: Border.all(color: terciaryColor, width: 1.0),
              color: _colorWidgetTwo,
            ),
            height: _heigthWidgetTwo,
            duration: Duration(milliseconds: 500),
            child: Center(child: Text(
              widget.opcaoTwo,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: secondaryColor
              ),
            )),
          ),
        )),
        SizedBox(height: 10, width: 3,),
      ],
    );
  }

  void selectedWidget(int widgetSelected) {
    setState(() {
      if(widgetSelected == 1){
        widget.selectedCallBack(true);

        _heigthWidgetOne = _heigthWidgetSelected;
        _heigthWidgetTwo = _heigthWidgetNormal;

        _flexOne = _flexSelected;
        _flexTwo = _flexNormal;

        _colorWidgetOne = _colorWidgetSelected;
        _colorWidgetTwo = _colorWidgetNormal;
      } else if(widgetSelected == 2){ ///
        widget.selectedCallBack(false);

        _heigthWidgetOne = _heigthWidgetNormal;
        _heigthWidgetTwo = _heigthWidgetSelected;

        _flexOne = _flexNormal;
        _flexTwo = _flexSelected;

        _colorWidgetOne = _colorWidgetNormal;
        _colorWidgetTwo = _colorWidgetSelected;
      }
    });
  }
}
