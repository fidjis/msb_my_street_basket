import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_auth_buttons/flutter_auth_buttons.dart';
import 'package:google_sign_in/google_sign_in.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          color: Colors.orange,
        ),
        Container(
          color: Colors.white70,
        ),
        Container(
          decoration: new BoxDecoration(
              color: const Color(0xFF66BB6A),
              boxShadow: [new BoxShadow(
                color: Colors.black,
                blurRadius: 20.0,
              ),]
          ),
          height: MediaQuery.of(context).size.height/3,
        ),
        Container(
          padding: EdgeInsets.only(top: MediaQuery.of(context).size.height/4),
          height: 400.0,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Card(
                color: Colors.white,
                child: Container(
                  padding: EdgeInsets.all(15.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[

                      GoogleSignInButton(
                        onPressed: () {
                          logginFirebase();
                        },
                        darkMode: true, // default: false
                      ),
                      TwitterSignInButton(
                        onPressed: () {

                        },
                        borderRadius: 10.0,
                      ),
                      FacebookSignInButton(
                        onPressed: () {},
                      ),

                    ],
                  ),
                )
              )
            ],
          )
        ),
      ],
    );
  }

  ///LOGIN

  final googleSignIn = GoogleSignIn();
  final auth = FirebaseAuth.instance;

  signUpFirebase() {

    logginFirebase();
  }

  Future<Null> logginFirebase() async{
    GoogleSignInAccount user = googleSignIn.currentUser;
    if(user == null)
      user = await googleSignIn.signInSilently();
    if(user == null)
      user = await googleSignIn.signIn();
    if(await auth.currentUser() == null){
      GoogleSignInAuthentication credentials = await googleSignIn.currentUser.authentication;
      await auth.signInWithGoogle(
          idToken: credentials.idToken,
          accessToken: credentials.accessToken
      );
    }
  }
}
