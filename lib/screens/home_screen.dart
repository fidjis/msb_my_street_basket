import 'package:flutter/material.dart';
import 'package:msb_my_street_basket/screens/game_screen.dart';
import 'package:msb_my_street_basket/screens/game_sethings_screen.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
//    return GameSethingsScreen();
    return Scaffold(
      body: Center(child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
//          FloatingActionButton.extended(heroTag: "tag1", icon: Icon(Icons.account_circle), label: Text("GameScreen"), onPressed: (){
//            Navigator.pushNamed(context, '/GameScreen');
//          }),
          Text("Versão: Beta 1.01"),
          SizedBox(height: 30.0,),
          FloatingActionButton.extended(
              heroTag: "tag2",
              backgroundColor: Colors.red[400],
              icon: Icon(Icons.offline_bolt),
              label: Text("Novo Jogo!"),
              onPressed: (){
                Navigator.pushNamed(context, '/GameSethingsScreen');
              }),


          SizedBox(height: 30.0,),
          FloatingActionButton.extended(
              heroTag: "tag4",
              backgroundColor: Colors.red[400],
              icon: Icon(Icons.person),
              label: Text("Login"),
              onPressed: (){
                Navigator.pushNamed(context, '/LoginScreen');
              }),
          SizedBox(height: 30.0,),
          FloatingActionButton.extended(
              heroTag: "tag5",
              backgroundColor: Colors.red[400],
              icon: Icon(Icons.person),
              label: Text("Login 2"),
              onPressed: (){
                Navigator.pushNamed(context, '/LoginScreen2');
              }),


        ],
      ),
      )
    );
  }

}
