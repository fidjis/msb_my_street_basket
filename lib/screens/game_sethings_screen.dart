import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:msb_my_street_basket/models/game_mode_info.dart';
import 'package:msb_my_street_basket/screens/game_screen.dart';
import 'package:msb_my_street_basket/widgets/diagonal_cliper.dart';
import 'package:msb_my_street_basket/widgets/select_between_two.dart';
import 'package:msb_my_street_basket/util/idiomas.dart';
import 'package:msb_my_street_basket/widgets/show_up_animation.dart';

class GameSethingsScreen extends StatefulWidget {
  @override
  _GameSethingsScreenState createState() => _GameSethingsScreenState();
}

Color secondaryColor = Colors.teal[900];
Color primaryColor = Colors.red[400];

class _GameSethingsScreenState extends State<GameSethingsScreen> with TickerProviderStateMixin{

  var _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool seteTimeSelected = false;

  PageController _pageController = PageController();
  GameModel gameModel = GameModel();
  int delayAmount = 500;

  int _numberPlayers = 2;
  double _bodyHeight = 0.0;
  bool _isVisible = false;

  @override
  void initState(){
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: primaryColor,
        title: Text("MSB", style: TextStyle(fontWeight: FontWeight.bold, color: secondaryColor),),
        leading: GestureDetector(
          onTap: (){Navigator.pop(context);},
          child: Icon(Icons.arrow_back_ios, color: secondaryColor),
        ),
        bottom: PreferredSize(
          preferredSize: Size(0.0, 130.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 15.0, bottom:30.0, top: 10.0),
                child: Text(
                  ConstsLinguage.gameModeTitle,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: secondaryColor,
                      fontSize: 40.0
                  ),
                ),
              ),
            ],
          )
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
          onPressed: (){
//            Navigator.pushNamed(context, '/GameScreen');
            if(seteTimeSelected)
              Navigator.push(context, MaterialPageRoute(
                builder: (BuildContext context) =>  GameScreen(gameModel),
              ));
              else
              _scaffoldKey.currentState.showSnackBar(
                  SnackBar(duration: Duration(milliseconds: 1500),content: Text("Selecione o tempo de jogo!")));
          },
          heroTag: "fab_game_sethings",
          backgroundColor: Colors.white,
          icon: Image.asset(
            "assets/imgs/boll_icon.png", //URL da img
            height: 30.0,
            width: 30.0,
            fit: BoxFit.contain,
          ),
          label: Text("Pronto!", style: TextStyle(
              fontWeight: FontWeight.bold,
              color: secondaryColor,
              fontSize: 17.0
          ))
      ),
      body: Stack(
        children: <Widget>[
          Image.asset(
            "assets/imgs/background_laranja.jpg", //UR
//            "assets/imgs/background_azul.jpg", //UR
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            fit: BoxFit.fill,
          ),
          ListView(
            children: <Widget>[
              Container(
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 20, width: 1,),
                    Text(
                        "Selecione as opções desejadas!",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: secondaryColor,
                            fontSize: 17.0
                        )),
                    SizedBox(height: 10, width: 1,),
                    ShowUp(
                      child: SelectBetweenTwo(
                        opcaoOne: "5 minutos",
                        opcaoTwo: "10 minutos",
                        selectedCallBack: (value){
                          seteTimeSelected = true;
                          if(value)
                            gameModel.seteDuration = Duration(minutes: 5);
                          else
                            gameModel.seteDuration = Duration(minutes: 10);
                        },
                      ),
                      delay: delayAmount-100,
                    ),
                    SizedBox(height: 10, width: 1,),
                    ShowUp(
                      child: Column(
                        children: <Widget>[
                          SelectBetweenTwo(
                            opcaoOne: "Selecionar Quant. Jogadores (sorteio)",
                            opcaoTwo: "Nao Selecionar (vcs escolhem)",
                            selectedCallBack: (value) async{
                              setState(() {
                                if(value){
                                  _bodyHeight = 100.0;
                                }else{
                                  this._isVisible = false;
                                  _bodyHeight = 0.0;
                                }
                              });
                              if(value)
                                await Timer(Duration(milliseconds: 501), (){
                                  setState(() => this._isVisible = value );
                                });
//                          selectPlayersNumber();
                            },
                          ),
                          AnimatedContainer(
                            child: Visibility(
                              maintainState: true,
                              visible: _isVisible,
                              child: Column(
                                children: <Widget>[
                                  SizedBox(height: 10, width: 1,),
                                  Container(
                                      decoration: BoxDecoration(
                                        border: Border.all(color: terciaryColor, width: 1.0),
                                        color: primaryColor,
                                      ),
                                      height: 90.0,
                                      child: Center(child: ListTile(
                                        onTap: (){_buildCupertinoPlayerPiker();},
                                        leading: Icon(Icons.people),
                                        title: Text("Quant. Jogadores: $_numberPlayers", style: TextStyle(color: secondaryColor),),
                                      ))
                                  ),
                                ],
                              ),
                            ),
                            curve: Curves.easeInOut,
                            duration: const Duration(milliseconds: 500),
                            height: _bodyHeight,
                            // color: primaryColor,
                          ),
                        ],
                      ),
                      delay: delayAmount-200,
                    ),
                    SizedBox(height: 10, width: 1,),
                    ShowUp(
                      delay: delayAmount,
                      child: SelectBetweenTwo(
                        opcaoOne: ConstsLinguage.wholeBlock,
                        opcaoTwo: ConstsLinguage.halfBlock,
                        selectedCallBack: (value){
                          selectedWidgetQuadra(value);
                        },
                      ),
                    ),
                    SizedBox(height: 10, width: 1,),
                    ShowUp(
                      delay: delayAmount+200,
                      child: SelectBetweenTwo(
                        opcaoOne: ConstsLinguage.boolPosseTrue,
                        opcaoTwo: ConstsLinguage.boolPosseFalse,
                        selectedCallBack: (value){
                          selectedWidgetBollPosse(value);
                        },
                      ),
                    ),
                    SizedBox(height: 80, width: 1,),
                  ],
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  List<Widget> itens;
  _buildCupertinoPlayerPiker(){
    itens = List<Widget>();
    for(int a = 2; a <= 25; a++){
      setState(() {
        itens.add(
            Card(child: Container(
                height: 50.0,
                width: 100.0,
                child: Center(child: Text("$a", style: TextStyle(color: secondaryColor,)),)
            ))
        );
      });
    }
    showModalBottomSheet<void>(context: context,
        builder: (BuildContext context) {
          return GestureDetector(
            onTap: null,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SizedBox(height: 5.0,),
                Center(child: Text(
                  "Selecione a quantidade de jogadores!",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: secondaryColor, fontSize: 17.0),
                )),
                Container(
                    height: MediaQuery.of(context).size.height / 2,
                    child: CupertinoPicker(
                      diameterRatio: 50,
                      magnification: 1.5,
                      backgroundColor: Colors.white,
                      children: itens,
                      itemExtent: 100, //height of each item
                      looping: true,
                      onSelectedItemChanged: (int index) {
                        setState(() {
                          _numberPlayers = index + 2; //POIS COMECA A PARTIR DE 2
                          print("Players: $_numberPlayers");
                        });
                      },
                    )),
              ],
            ),
          );
        });
  }


  void selectedWidgetQuadra(bool value) {
    gameModel.quadraToda = value;
    print("Quadra Toda: $value");
  }

  void selectedWidgetBollPosse(bool value) {
    gameModel.posseBola = value;
    print("Passe de Bola: $value");
  }

  int pageIndicator = 0;
  ///QUADRA TODA
  _buildPageOne(){
    return Stack(
      children: <Widget>[
        Positioned(top: 0.0 , child: GestureDetector(
          onTap: (){
            _pageController.jumpToPage(pageIndicator++);
            gameModel.quadraToda = true;
          },
          child: ClipPath(
              clipper: DialogonalClipper(),
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.purple[900],
                ),
                height: 284,
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: EdgeInsets.only(right: 25.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Text(
                            ConstsLinguage.wholeBlock,
                            textAlign: TextAlign.end,
                            style: TextStyle(
//                                color: secondaryColor,
                                color: Colors.green[300],
                                fontSize: 55.0
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Text(
                            ConstsLinguage.oneBasket,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
//                                color: secondaryColor,
                                color: Colors.green[300],
                                fontSize: 20.0
                            ),
                          ),
                        ],
                      )
                    ],
                  )
                ),
              )
          ),
        )),
        Positioned(bottom: 0.0, child: GestureDetector(
          onTap: (){
            _pageController.jumpToPage(pageIndicator++);
            gameModel.quadraToda = false;
          },
          child: ClipPath(
              clipper: DialogonalClipperReverse(),
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.green[900],
                    borderRadius: BorderRadius.circular(5.0)),
                height: 284,
                width: MediaQuery.of(context).size.width,
                child: Stack(
                  children: <Widget>[
                    Positioned(bottom: 0.0, child: Container(
                      padding: EdgeInsets.only(left: 18.0, bottom: 10.0),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            ConstsLinguage.halfBlock,
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                color: Colors.purple[300],
                                fontSize: 55.0
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                ConstsLinguage.twoBasket,
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    color: Colors.purple[300],
                                    fontSize: 20.0
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    )),
                  ],
                )
              )
          ),
        )),
        Center(child: Transform.rotate(
          angle: 0.245,
          child: Container(
            height: 100.0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: 10, width: 40, child: Container(margin: EdgeInsets.all(2.0), color: Colors.green,),),
                SizedBox(height: 10, width: 40, child: Container(margin: EdgeInsets.all(2.0), color: Colors.deepPurple,),),
                SizedBox(height: 10, width: 40, child: Container(margin: EdgeInsets.all(2.0), color: Colors.tealAccent,),),
                SizedBox(height: 10, width: 40, child: Container(margin: EdgeInsets.all(2.0), color: Colors.green,),),
                SizedBox(height: 10, width: 40, child: Container(margin: EdgeInsets.all(2.0), color: Colors.deepPurple,),),
                SizedBox(height: 10, width: 40, child: Container(margin: EdgeInsets.all(2.0), color: Colors.tealAccent,),),
                Image.asset(
                  "assets/imgs/boll_icon.png", //URL da img
                  height: 50.0,
                  width: 50.0,
                  fit: BoxFit.contain,
                ),
              ],
            )
          ),
        )),
      ],
    );
  }

  ///POSSE DE BOLA
  _buildPageTwo(){
    return Stack(
      children: <Widget>[
        Positioned(top: 0.0 , child: GestureDetector(
          onTap: (){
            _pageController.jumpToPage(pageIndicator++);
            gameModel.posseBola = true;
          },
          child: ClipPath(
              clipper: DialogonalClipper(),
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.green[900],
                ),
                height: 284,
                width: MediaQuery.of(context).size.width,
                child: Padding(
                    padding: EdgeInsets.only(right: 25.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Text(
                              ConstsLinguage.wholeBlock,
                              textAlign: TextAlign.end,
                              style: TextStyle(
                                  color: Colors.purple[300],
                                  fontSize: 55.0
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Text(
                              ConstsLinguage.oneBasket,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
//                                color: secondaryColor,
                                  color: Colors.green[300],
                                  fontSize: 20.0
                              ),
                            ),
                          ],
                        )
                      ],
                    )
                ),
              )
          ),
        )),
        Positioned(bottom: 0.0, child: GestureDetector(
          onTap: (){
            _pageController.jumpToPage(pageIndicator++);
            gameModel.posseBola = false;
          },
          child: ClipPath(
              clipper: DialogonalClipperReverse(),
              child: Container(
                  decoration: BoxDecoration(
                      color: Colors.purple[900],
                      borderRadius: BorderRadius.circular(5.0)),
                  height: 284,
                  width: MediaQuery.of(context).size.width,
                  child: Stack(
                    children: <Widget>[
                      Positioned(bottom: 0.0, child: Container(
                        padding: EdgeInsets.only(left: 18.0, bottom: 10.0),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              ConstsLinguage.halfBlock,
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                  color: Colors.green[300],
                                  fontSize: 55.0
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  ConstsLinguage.twoBasket,
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                      color: Colors.purple[300],
                                      fontSize: 20.0
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      )),
                    ],
                  )
              )
          ),
        )),
        Center(child: Transform.rotate(
          angle: 0.245,
          child: Container(
              height: 100.0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(height: 10, width: 40, child: Container(margin: EdgeInsets.all(2.0), color: Colors.green,),),
                  SizedBox(height: 10, width: 40, child: Container(margin: EdgeInsets.all(2.0), color: Colors.deepPurple,),),
                  SizedBox(height: 10, width: 40, child: Container(margin: EdgeInsets.all(2.0), color: Colors.tealAccent,),),
                  SizedBox(height: 10, width: 40, child: Container(margin: EdgeInsets.all(2.0), color: Colors.green,),),
                  SizedBox(height: 10, width: 40, child: Container(margin: EdgeInsets.all(2.0), color: Colors.deepPurple,),),
                  SizedBox(height: 10, width: 40, child: Container(margin: EdgeInsets.all(2.0), color: Colors.tealAccent,),),
                  Image.asset(
                    "assets/imgs/boll_icon.png", //URL da img
                    height: 50.0,
                    width: 50.0,
                    fit: BoxFit.contain,
                  ),
                ],
              )
          ),
        )),
      ],
    );
  }
}