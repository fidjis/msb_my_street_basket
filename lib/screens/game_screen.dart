import 'dart:async';

import 'package:msb_my_street_basket/models/game_mode_info.dart';
import 'package:flutter/material.dart';
import 'package:audioplayers/audio_cache.dart';
import 'package:msb_my_street_basket/util/timer/egg_timer.dart';
import 'package:msb_my_street_basket/util/timer/timerSeteDisplay.dart';
import 'package:screen/screen.dart';

class GameScreen extends StatefulWidget {

  final GameModel gameModel;

  GameScreen(this.gameModel);

  @override
  _GameScreenState createState() => _GameScreenState();
}

Color secondaryColor = Colors.teal[900];

class _GameScreenState extends State<GameScreen> {

  var _scaffoldKey = new GlobalKey<ScaffoldState>();

  int scoreTimeOne = 0;
  int scoreTimeTwo = 0;
  int timeOfSet = 600; //10 minutos em segundos
  String timeOfSetString;
  int timeOfbollPosse = 24;

  bool timeOfSetPressed = false;
  bool timerOfbollPossePressed = false;

  List<String> timeOne = ["Jogador 1","Jogador 2","Jogador 3","Jogador 4","Jogador 5"];
  List<String> timeTwo = ["Jogador 1","Jogador 2","Jogador 3","Jogador 4","Jogador 5"];

  String labelFAB = "Iniciar o Jogo" ;

  @override
  void initState() {
    super.initState();
    _onTimeSelected(widget.gameModel.seteDuration);
    print(widget.gameModel.quadraToda);
    Screen.keepOn(true);
    Timer(Duration(milliseconds: 500), (){_showDialogAlertOfBugs();});
  }

  ///AUDIO
  static AudioCache player = new AudioCache();
  _playSoundPi(){
    const alarmAudioPath = "sounds/sound_pi.mp3";
    player.play(alarmAudioPath);
  }

  ///TIMER

  EggTimer eggTimer;

  _GameScreenState() {
    eggTimer = new EggTimer(
      maxTime: const Duration(minutes: 10),
      onTimerUpdate: _onTimerUpdate,
    );
  }

  _onTimeSelected(Duration newTime) {
    setState(() {
      eggTimer.currentTime = newTime;
    });
  }

  _onTimerUpdate() {
    setState(() { });
  }

  ///TIMER

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: Colors.red[400],
        title: Text("MSB", style: TextStyle(fontWeight: FontWeight.bold, color: secondaryColor),),
        leading: GestureDetector(
          onTap: (){
            Screen.keepOn(false);
            Navigator.pop(context);
          },
          child: Icon(Icons.arrow_back_ios, color: secondaryColor),
        ),
        bottom: PreferredSize(
            preferredSize: Size(0.0, 110.0),
            child: Padding(padding: EdgeInsets.only(bottom: 10.0),
                child: TimerSeteDisplay(
                  eggTimerState: eggTimer.state,
                  selectionTime: eggTimer.lastStartTime,
                  countdownTime: eggTimer.currentTime,
                ))
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
          onPressed: startTimerOfSete,
          heroTag: "fab_game_screen",
          backgroundColor: Colors.white,
          icon: Image.asset(
            "assets/imgs/boll_icon.png", //URL da img
            height: 30.0,
            width: 30.0,
            fit: BoxFit.contain,
          ),
          label: Text(labelFAB, style: TextStyle(
              fontWeight: FontWeight.bold,
              color: secondaryColor,
              fontSize: 17.0
          ))
      ),
      body: Stack(
        children: <Widget>[
          Image.asset(
            "assets/imgs/background_laranja.jpg", //UR
//            "assets/imgs/background_azul.jpg", //UR
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            fit: BoxFit.fill,
          ),
          Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SizedBox(height: 10.0,),
              Row(
                children: <Widget>[
                  Expanded(child: Container(
                    padding: EdgeInsets.all(5.0),
                    child: Row(
                      children: <Widget>[
                        Expanded(child: _buildImgTime1()),
                        Expanded(child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Expanded(child: Text(
                                  "$scoreTimeOne",
                                  textAlign: TextAlign.end,
                                  style: TextStyle(
                                      color: secondaryColor,
                                      fontSize: 24.0,
                                      fontWeight: FontWeight.bold
                                  ),
                                )),
                                Text(
                                  " - ",
                                  style: TextStyle(
                                      color: secondaryColor,
                                      fontSize: 20.0,
                                      fontWeight: FontWeight.bold
                                  ),
                                ),
                                Expanded(child: Text(
                                  "$scoreTimeTwo",
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                      color: secondaryColor,
                                      fontSize: 24.0,
                                      fontWeight: FontWeight.bold
                                  ),
                                ))
                              ],
                            ),
                            Container(
                              padding: EdgeInsets.all(7.0),
                              margin: EdgeInsets.all(4.0),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(30.0),
                                color: secondaryColor,
                              ),
                              child: Text(
                                "PLACAR",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 14.0,
                                    fontWeight: FontWeight.bold
                                ),
                              ),
                            )
                          ],
                        ),),
                        Expanded(child: _buildImgTime2()),
                      ],
                    ),
                  ))
                ],
              ),
              SizedBox(height: 10.0,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Flexible(child: Text("(futura linha do tempo: time X marcou 2, time Y marcou 1)",))
                ],
              ),
              Row(
                children: <Widget>[
                  Expanded(child: Card(
                    color: Colors.red[400],
                    child: Container(
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              IconButton(icon: Icon(Icons.add, color: Colors.white),onPressed: (){addScore(time: 1, score: 1);},),
                              Text("1", style: TextStyle(color: Colors.white),),
                              IconButton(icon: Icon(Icons.remove, color: Colors.white),onPressed: (){removeScore(time: 1, score: 1);},),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              IconButton(icon: Icon(Icons.add, color: Colors.white),onPressed: (){addScore(time: 1, score: 2);},),
                              Text("2", style: TextStyle(color: Colors.white),),
                              IconButton(icon: Icon(Icons.remove, color: Colors.white),onPressed: (){removeScore(time: 1, score: 2);},),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              IconButton(icon: Icon(Icons.add, color: Colors.white),onPressed: (){addScore(time: 1, score: 3);},),
                              Text("3", style: TextStyle(color: Colors.white),),
                              IconButton(icon: Icon(Icons.remove, color: Colors.white),onPressed: (){removeScore(time: 1, score: 3);},),
                            ],
                          )
                        ],
                      ),
                    ),
                  )),
                  Expanded(child: Card(
                    color: Colors.red[400],
                    child: Container(
                        padding: EdgeInsets.all(5.0),
                        child: Column(
                          children: <Widget>[
                            Text(
                              "$timeOfbollPosse",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: secondaryColor, //TODO: mudar as cores e emitir avisos conforme o tempo que falta
                                  fontSize: 50.0
                              ),
                            ),
                            Text(
                              "segundos",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: secondaryColor,
                                  fontSize: 20.0
                              ),
                            ),
                            SizedBox(height: 7.0,),
                            FloatingActionButton.extended(
                                backgroundColor: Colors.white,
                                elevation: 5.0,
                                onPressed: (){
                                  _scaffoldKey.currentState.showSnackBar(
                                      SnackBar(duration: Duration(milliseconds: 1500),content: Text("Nao Implementado!")));
                                  },
                                icon: Icon(Icons.refresh, color: secondaryColor),
                                label: Text("reiniciar", style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: secondaryColor,
                                    fontSize: 10.0
                                ))
                            )
                          ],
                        )
                    ),
                  )),
                  Expanded(child: Card(
                    color: Colors.red[400],
                    child: Container(
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              IconButton(icon: Icon(Icons.add, color: Colors.white),onPressed: (){addScore(time: 2, score: 1);},),
                              Text("1", style: TextStyle(color: Colors.white),),
                              IconButton(icon: Icon(Icons.remove, color: Colors.white),onPressed: (){removeScore(time: 2, score: 1);},),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              IconButton(icon: Icon(Icons.add, color: Colors.white),onPressed: (){addScore(time: 2, score: 2);},),
                              Text("2", style: TextStyle(color: Colors.white),),
                              IconButton(icon: Icon(Icons.remove, color: Colors.white),onPressed: (){removeScore(time: 2, score: 2);},),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              IconButton(icon: Icon(Icons.add, color: Colors.white),onPressed: (){addScore(time: 2, score: 3);},),
                              Text("3", style: TextStyle(color: Colors.white),),
                              IconButton(icon: Icon(Icons.remove, color: Colors.white),onPressed: (){removeScore(time: 2, score: 3);},),
                            ],
                          )
                        ],
                      ),
                    ),
                  )),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("jogadores")
                ],
              ),
              Row(
                children: <Widget>[
                  Expanded(child: ListView.builder(
                      itemCount: timeOne.length,
                      shrinkWrap: true,
                      itemBuilder: (BuildContext ctxt, int index) {
                        return new Text(timeOne[index], textAlign: TextAlign.center,);
                      }
                  )),
                  Expanded(child: ListView.builder(
                      itemCount: timeTwo.length,
                      shrinkWrap: true,
                      itemBuilder: (BuildContext ctxt, int index) {
                        return new Text(timeTwo[index], textAlign: TextAlign.center,);
                      }
                  ))
                ],
              )
            ],
          ),
        ],
      ),
    );
  }

  void _showDialogAlertOfBugs() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Center(child: Text("AVISO!"),),
          content: SizedBox(
            height: 100.0,
            child: Center(
              child: Text("Algumas opções escolhidas na tela anterior ainda não podem ser implementadas!\n\n\nVersao: Beta 1.01"),),
          ),
          actions: <Widget>[
            new FlatButton(
              child: new Text("ok", style: TextStyle(color: Colors.red),),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }


  Timer _timerOfbollPosse;

  @override
  void dispose() {

    try{
      _timerOfbollPosse.cancel();
    }catch(Exeption){}

    super.dispose();
  }

  void startTimerOfbollPosse() {
    if(!timerOfbollPossePressed){
      timerOfbollPossePressed = true;
      const oneSec = const Duration(seconds: 1);
      _timerOfbollPosse = Timer.periodic(
          oneSec,
              (Timer timer) => setState(() {
            if (timeOfbollPosse < 1) {
              timer.cancel();
              timerOfbollPossePressed = false;
            } else {
              timeOfbollPosse = timeOfbollPosse - 1;
            }
            if(timeOfbollPosse <= 10)
              _playSoundPi();
          }));
    }else{
      _timerOfbollPosse.cancel();
      timeOfbollPosse = 24;
      const oneSec = const Duration(seconds: 1);
      _timerOfbollPosse = Timer.periodic(
          oneSec,
              (Timer timer) => setState(() {
            if (timeOfbollPosse < 1) {
              timer.cancel();
              timerOfbollPossePressed = false;
            } else {
              timeOfbollPosse = timeOfbollPosse - 1;
            }
          }));
    }
  }

  void startTimerOfSete(){
    if(!timeOfSetPressed){
      labelFAB = "Pausar o jogo";
      timeOfSetPressed = true;
      setState(() => eggTimer.resume());

    }else{ //pausar
      timeOfSetPressed = false;
      labelFAB = "Continuar o Jogo" ;
      setState(() => eggTimer.pause());
    }
  }

  _buildImgTime1() {
    return Image.asset(
      "assets/imgs/time_1.png", //URL da img
//      height: 50.0,
//      width: 50.0,
      fit: BoxFit.contain,
    );
  }

  _buildImgTime2() {
    return Image.asset(
      "assets/imgs/time_2.png", //URL da img
//      height: 50.0,
//      width: 50.0,
      fit: BoxFit.contain,
    );
  }

  void addScore({@required int time, @required int score}) {
    setState(() {
      switch(time){
        case 1:
          scoreTimeOne += score;
          break;
        case 2:
          scoreTimeTwo += score;
          break;
      }
    });
  }

  void removeScore({@required int time, @required int score}) {
    setState(() {
      switch(time){
        case 1:
          if(scoreTimeOne >= score)
            scoreTimeOne -= score;
          break;
        case 2:
          if(scoreTimeTwo >= score)
            scoreTimeTwo -= score;
          break;
      }
    });
  }
}
