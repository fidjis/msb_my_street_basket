import 'package:flutter/material.dart';
import 'package:msb_my_street_basket/screens/game_screen.dart';
import 'package:msb_my_street_basket/screens/game_sethings_screen.dart';
import 'package:msb_my_street_basket/screens/home_screen.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:msb_my_street_basket/screens/login_screen.dart';
import 'package:msb_my_street_basket/screens/login_screen_2.dart';
import 'package:msb_my_street_basket/util/idiomas.dart';

void main() => runApp(MyApp());

/// Para usar a localizacao : AppLocalizations.instance.text('page_one')
/// https://github.com/billylev/flutter_localizations
/// https://github.com/flutter/flutter/tree/master/packages/flutter_localizations/lib/src/l10n
/// https://medium.com/@datvt9312/flutter-internationalization-tutorials-d8f0f711e7f

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
//      title: 'MSB - My Street Basket',
      title: 'MSB',
      localizationsDelegates: [
        // ... app-specific localization delegate[s] here
        const AppLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('pt', 'BR'), // Portugues - Brasil
        const Locale('en', 'US'), // English - EUA
        // ... other locales the app supports
      ],
      localeResolutionCallback:
          (Locale locale, Iterable<Locale> supportedLocales) {
        for (Locale supportedLocale in supportedLocales) {
          if (supportedLocale.languageCode == locale.languageCode ||
              supportedLocale.countryCode == locale.countryCode) {
            return supportedLocale;
          }
        }
        return supportedLocales.first;
      },
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      routes: {
        '/GameScreen': (context) => GameScreen(null),
        '/GameSethingsScreen': (context) => GameSethingsScreen(),
        '/LoginScreen': (context) => LoginPage(),
        '/LoginScreen2': (context) => LoginScreen(),
      },
      home: HomeScreen(),
    );
  }
}
