import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:msb_my_street_basket/util/timer/egg_timer.dart';

class TimerSeteDisplay extends StatefulWidget {

  final eggTimerState;
  final selectionTime;
  final countdownTime;

  TimerSeteDisplay({
    this.eggTimerState,
    this.selectionTime = const Duration(seconds: 0),
    this.countdownTime = const Duration(seconds: 0),
  });

  @override
  _TimerSeteDisplayState createState() => new _TimerSeteDisplayState();
}

class _TimerSeteDisplayState extends State<TimerSeteDisplay> with TickerProviderStateMixin {

  final DateFormat selectionTimeFormat = new DateFormat('mm');
  final DateFormat countdownTimeFormat = new DateFormat('mm:ss');

  AnimationController selectionTimeSlideController;
  AnimationController countdownTimeFadeController;

  @override
  void initState() {
    super.initState();

    selectionTimeSlideController = new AnimationController(
      duration: const Duration(milliseconds: 150),
      vsync: this,
    )
      ..addListener(() {
        setState(() {});
      });

    countdownTimeFadeController = new AnimationController(
      duration: const Duration(milliseconds: 150),
      vsync: this,
    )
      ..addListener(() {
        setState(() {});
      });
    countdownTimeFadeController.value = 1.0;
  }

  @override
  void dispose() {
    selectionTimeSlideController.dispose();
    countdownTimeFadeController.dispose();
    super.dispose();
  }

  get formattedSelectionTime {
    DateTime dateTime = new DateTime(new DateTime.now().year, 0, 0, 0, 0, widget.selectionTime.inSeconds);
    return selectionTimeFormat.format(dateTime);
  }

  get formattedCountdownTime {
    DateTime dateTime = new DateTime(new DateTime.now().year, 0, 0, 0, 0, widget.countdownTime.inSeconds);
    return countdownTimeFormat.format(dateTime);
  }

  @override
  Widget build(BuildContext context) {

    if (widget.eggTimerState == EggTimerState.ready) {
      selectionTimeSlideController.reverse();
      countdownTimeFadeController.forward();
    } else {
      selectionTimeSlideController.forward();
      countdownTimeFadeController.reverse();
    }

    return new Padding(
      padding: const EdgeInsets.only(top: 15.0),
      child: new Stack(
        alignment: Alignment.center,
        children: [
          Row(
            children: <Widget>[
              Expanded(child: Container(
//                  padding: EdgeInsets.all(5.0),
                  child: Column(
                    children: <Widget>[
                      Text(
                        formattedCountdownTime,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.teal[900],  //TODO: mudar as cores e emitir avisos conforme o tempo que falta
                            fontSize: 55.0
                        ),
                      ),
                      Text(
                        "periodo",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.teal[900],
                            fontSize: 20.0
                        ),
                      ),
                    ],
                  )
              ),)
            ],
          ),
        ],
      ),
    );
  }
}