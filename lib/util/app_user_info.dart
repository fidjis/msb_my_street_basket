import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:msb_my_street_basket/screens/user_profile_screen.dart';

class AppUserInfo {

  static final UserInfo _instance = AppUserInfo.internal();

  factory AppUserInfo() => _instance;

  AppUserInfo.internal();

  final googleSignIn = GoogleSignIn();
  final auth = FirebaseAuth.instance;

  Future<Null> logginFirebase(Function CallBackGoToUserPage) async{

    //TODO: IMPLEMENTAR UM PRoGRESS e controle de internet

    GoogleSignInAccount user = googleSignIn.currentUser;
    if(user == null){ //login silencioso
      user = await googleSignIn.signInSilently();
    }
    if(user == null)
      user = await googleSignIn.signIn();
    if(await auth.currentUser() == null){
      GoogleSignInAuthentication credentials = await googleSignIn.currentUser.authentication;
      await auth.signInWithGoogle(
          idToken: credentials.idToken,
          accessToken: credentials.accessToken
      );
    }
    if(user != null)
      CallBackGoToUserPage();
  }

}