import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AppLocalizations {

  static final AppLocalizations _singleton = new AppLocalizations._internal();
  AppLocalizations._internal();
  static AppLocalizations get instance => _singleton;

  Map<dynamic, dynamic> _localisedValues;

  Future<AppLocalizations> load(Locale locale) async {
    String jsonContent = await rootBundle.loadString("assets/localizations/localization_${locale.languageCode}.json");
    _localisedValues = json.decode(jsonContent);
    return this;
  }

  String text(String key) {
    return _localisedValues[key] ?? "$key not found";
  }
}

class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'pt'].contains(locale.languageCode);

  @override
  Future<AppLocalizations> load(Locale locale)  {
    return AppLocalizations.instance.load(locale);
  }

  @override
  bool shouldReload(AppLocalizationsDelegate old) => true;
}

class ConstsLinguage{

  static String gameTitle = AppLocalizations.instance.text('gameTitle');
  static String gameModeTitle = AppLocalizations.instance.text('gameModeTitle');
  static String wholeBlock = AppLocalizations.instance.text('wholeBlock');
  static String halfBlock = AppLocalizations.instance.text('halfBlock');
  static String oneBasket = AppLocalizations.instance.text('oneBasket');
  static String twoBasket = AppLocalizations.instance.text('twoBasket');
  static String boolPosseTrue = AppLocalizations.instance.text('boolPosseTrue');
  static String boolPosseFalse = AppLocalizations.instance.text('boolPosseFalse');
}